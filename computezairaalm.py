#!/usr/bin/python3
# -*- coding: utf-8 -*-

import computecount


class ComputeCalls(computecount.Compute):  # Creamos clase heredadda de computecount, por lo que no necesitamos el init

	def calls(self):
		return self.count
