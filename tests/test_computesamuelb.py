#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import computesamuelb


class TestComputesamuelb(unittest.TestCase):

    compute = computesamuelb.ComputeCalls()

    def test_count(self):
        self.compute.power(2, 3)
        self.compute.power(2, 4)
        self.compute.log(8, 2)
        self.assertEqual(self.compute.calls(), 3)


if __name__ == '__main__':
    unittest.main()
