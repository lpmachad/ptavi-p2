#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import computezairaalm


class Testcomputezairaalm(unittest.TestCase):  # Creamos clase con su determinado nombre.

	compute = computezairaalm.ComputeCalls()  # Instanciamos un objeto para utilizarlo después en el test.

	def test_count(self):  # Llamamos tres veces a las funciones para comprobar que funciona el count.
		self.compute.log(8, 2)
		self.compute.power(2, 4)
		self.compute.log(24, 2)
		self.assertEqual(self.compute.calls(), 3)


if __name__ == '__main__':  # Ejecutamos el programa

	unittest.main()
