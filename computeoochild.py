#!/usr/bin/python3
# -*- coding: utf-8 -*-

import computeoo    # Importamos el fichero computeoo


class ComputeChild(computeoo.Compute):    # En este caso esta clase no está vacía, nos referimos al fichero y a la clase

    def set_def(self, num2):
        if num2 is None:    # num2 está definido en computeoo como el valor de la base y el exponente
            return self.default
        if num2 <= 0:
            raise (ValueError('Error: third argument should be a number'))

    def get_def(self):
        return self.default


objeto1 = ComputeChild()
print('The default number: ', objeto1.get_def())
